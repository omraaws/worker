import logging
import os
import signal
import subprocess
import time

from pymongo import MongoClient

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
log = logging.getLogger(__name__)


class Status:
    NEW = 'new'
    PROCESSING = 'processing'
    REFUSED = 'refused'
    BLOCKED = 'blocked'
    DONE = 'done'

class Format:
    XML = 'xml'
    PDF = 'pdf'
    MIDI = 'mid'

class Image:
    TARGET_DIRECTORY = 'target_directory'
    TARGET_PATH = 'target_path'
    # ORIGINAL_FILENAME = 'original_filename'
    # MIMETYPE = 'mimetype'
    # GUESSED_EXTENSION = 'guessed_extension'
    # TARGET_FILENAME = 'target_filename'
    # DELETE_TOKEN = 'delete_token'

class Task:
    IMAGE = 'image'
    STATUS = 'status'
    OUTPUT_FORMAT = 'output_format'
    DELETE_TOKEN = 'delete_token'
    BLOCKS = 'blocks'
    REASON = 'reason'

class Artifact:
    IMAGE = 'image'
    FORMAT = 'format'
    FILENAME = 'filename'
    DIRECTORY = 'directory'
    PATH = 'path'

class RefusedError(Exception):
    pass

class BlockedError(Exception):
    pass

class Worker:
    def __init__(self, db, delay=2):
        self.db = db
        self.delay = delay

    def work(self):
        while True:
            task = self.db.queue.find_one_and_update(
                {Task.STATUS: Status.NEW},
                {'$set': {Task.STATUS: Status.PROCESSING}})
            if task is None:
                time.sleep(self.delay)
                continue

            log.info("Got new task {id}: convert {image} to {format}".format(
                id=task['_id'], image=task[Task.IMAGE], format=task[Task.OUTPUT_FORMAT].upper()))
            output_format = task[Task.OUTPUT_FORMAT]

            try:
                image = self.get_image(task[Task.IMAGE])
                artifact = self.get_artifact(task[Task.IMAGE], task[Task.OUTPUT_FORMAT], throw=False)

                if artifact:
                    log.info('Artifact already exists: %s', artifact[Artifact.PATH])
                else:   # let's create it!
                    if output_format == Format.XML:
                        self.convert_to_xml(task, image)
                    elif output_format == Format.PDF:
                        self.convert_to_pdf(task, image)
                    elif output_format == Format.MIDI:
                        self.convert_to_midi(task, image)
                    else:
                        raise RefusedError('Incorrect output format')
            except RefusedError as err:
                self.db.queue.update_one({'_id': task['_id']}, {'$set': {
                    Task.STATUS: Status.REFUSED,
                    Task.REASON: err.message,
                }})
                log.info('Task refused: %s', err.message)
            except BlockedError as err:
                self.db.queue.update_one({'_id': task['_id']}, {'$set': {
                    Task.STATUS: Status.BLOCKED,
                    Task.REASON: err.message,
                }})
                log.info('Task blocked: %s', err.message)
            else: # finished successfully
                self.db.queue.update_one({'_id': task['_id']}, { '$set': {
                    Task.STATUS: Status.DONE,
                }})
                log.info('Task complete')

                if task.get(Task.BLOCKS):
                    result = self.db.queue.update_one({'_id': task[Task.BLOCKS]}, {
                        '$set': {Task.STATUS: Status.NEW},
                        '$unset': {Task.REASON: ''},
                    })
                    log.info('Task unblocked other task %s', task[Task.BLOCKS])

            time.sleep(self.delay)

        log.info('Worker exiting...')

    @staticmethod
    def prepare_artifact(task, image):
        artifact_id = "{image}.{format}".format(image=image['_id'], format=task[Task.OUTPUT_FORMAT])
        filename = 'artifact.%s' % task[Task.OUTPUT_FORMAT]
        directory = image[Image.TARGET_DIRECTORY]
        path = os.path.join(directory, filename)
        artifact = {
            '_id': artifact_id,
            Artifact.IMAGE: image['_id'],
            Artifact.FORMAT: task[Task.OUTPUT_FORMAT],
            Artifact.FILENAME: filename,
            Artifact.DIRECTORY: directory,
            Artifact.PATH: path,
        }
        return artifact

    def convert_to_xml(self, task, image):
        artifact = self.prepare_artifact(task, image)
        return_code = subprocess.call([
            'audiveris',
            '-batch',
            '-input', image[Image.TARGET_PATH],
            '-export', artifact[Artifact.PATH],
        ])
        if return_code != 0:
            raise RefusedError('Could not convert to %s', task[Task.OUTPUT_FORMAT].upper())
        log.info("Created artifact %s", artifact)
        self.db.artifacts.insert_one(artifact)

    def convert_to_pdf(self, task, image):
        artifact = self.prepare_artifact(task, image)
        return_code = subprocess.call([
            'audiveris',
            '-batch',
            '-input', image[Image.TARGET_PATH],
            '-print', artifact[Artifact.PATH],
        ])
        if return_code != 0:
            raise RefusedError('Could not convert to %s', task[Task.OUTPUT_FORMAT].upper())
        log.info("Created artifact %s", artifact)
        self.db.artifacts.insert_one(artifact)

    def convert_to_midi(self, task, image):
        xml_artifact = self.get_artifact(task[Task.IMAGE], Format.XML, throw=False)
        if not xml_artifact:
            task = {
                Task.IMAGE: task[Task.IMAGE],
                Task.STATUS: Status.NEW,
                Task.OUTPUT_FORMAT: Format.XML,
                Task.BLOCKS: task['_id'],
            }
            result = self.db.queue.insert_one(task)
            raise BlockedError('XML artifact needs to be created, see task %s' % str(result.inserted_id))

        midi_artifact = self.prepare_artifact(task, image)
        return_code = subprocess.call([
            'xvfb-run', 'mscore', xml_artifact[Artifact.PATH],
            '-o', midi_artifact[Artifact.PATH],
        ])
        if return_code != 0:
            raise RefusedError('Could not convert to %s', task[Task.OUTPUT_FORMAT].upper())
        log.info("Created artifact %s", midi_artifact)
        self.db.artifacts.insert_one(midi_artifact)

    def get_image(self, image_hash, throw=True):
        image = self.db.images.find_one({'_id': image_hash})
        if image is None and throw:
            raise RefusedError('Image %(image_hash)s does not exist' % locals())
        return image

    def get_artifact(self, image, format, throw=True):
        artifact_id = "%(image)s.%(format)s" % locals()
        artifact = self.db.artifacts.find_one({'_id': artifact_id})
        if artifact is None and throw:
            raise RefusedError('%(format)s artifact for image %(image)s does not exist' % locals())
        return artifact

if __name__ == '__main__':
    client = MongoClient('mongo', 27017)
    log.info('Connected to database')

    worker = Worker(db=client.app)
    worker.work()
