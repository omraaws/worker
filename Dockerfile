FROM omraaws/audiveris
MAINTAINER Maciej Gamrat

RUN apt-get update -qq && \
    apt-get install -qq ghostscript python-pip xvfb && \
    apt-get -t jessie-backports install -qq musescore && \
    rm -rf /var/lib/apt/lists/*

ADD . /usr/src/app
WORKDIR /usr/src/app
RUN pip install -r requirements.txt

ENTRYPOINT ["/usr/bin/env", "python"]
CMD ["worker.py"]
